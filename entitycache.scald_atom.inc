<?php

/**
 * @file
 * Scald atom entity controller with persistent cache.
 */

/**
 * Scald atom entity controller with persistent cache.
 */
class EntityCacheScaldAtomController extends ScaldAtomController {

  /**
   * {@inheritdoc}
   */
  public function resetCache(array $ids = NULL) {
    EntityCacheControllerHelper::resetEntityCache($this, $ids);
    parent::resetCache($ids);
  }

  /**
   * {@inheritdoc}
   */
  public function load($ids = array(), $conditions = array()) {
    return EntityCacheControllerHelper::entityCacheLoad($this, $ids, $conditions);
  }

}
